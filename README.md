# Backend application readme

This application is built upon Spring Boot and Gradle.
Use gradle commands to build/run this application or use provided IntelliJ IDEA project.
Application is not yet configurable, so all hosts are hardcoded into properties file.

> Please note that you have to configure your hosts file to include db-host and kafka-host entries.

## Requirements

1) Java >11;
2) Gradle >4.10.*.

## How to build
1. `gradle build -x test` or `gradle build` to run tests.

## How to run application

You have several options to run this application:
1. Pass java jar manually: `java -jar build/libs/chat-backend-$VERSION-SNAPSHOT.jar` with application arguments using `-Darg=1`;
    * Do the same for dev profile with debug on: `java -Dspring.profiles.active=dev -jar build/libs/chat-backend-0.0.2-SNAPSHOT.jar`.
2. `gradle bootRun` (not recommended);
3. Run corresponding bash scripts: `./run.sh` or `./run-dev.sh`.